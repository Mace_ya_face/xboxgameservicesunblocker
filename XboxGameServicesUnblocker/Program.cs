﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.ServiceProcess;
using System.Management;
using Microsoft.Win32;
using NetFwTypeLib;
using Newtonsoft.Json;

namespace XboxGameServicesUnblocker
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "XboxGameServicesUnblocker [ALPHA]";
            Menu();
        }

        static void Menu()
        {
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("XboxGameServicesUnblocker [ALPHA]");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n### Menu ###");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" 1 ");
            Console.SetCursorPosition(3, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("--> Unblock services");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" 2 ");
            Console.SetCursorPosition(3, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("--> Restore protections");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" 3 ");
            Console.SetCursorPosition(3, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("--> Create new hosts file backup");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" 4 ");
            Console.SetCursorPosition(3, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("--> Restore hosts file backup");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" 5 ");
            Console.SetCursorPosition(3, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("--> Visit GitLab page");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" 6 ");
            Console.SetCursorPosition(3, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("--> Exit");

            retryUserMenu:
            var userInput = Console.ReadKey().Key;

            switch (userInput)
            {
                case ConsoleKey.D1:
                    Console.Clear();
                    if (File.Exists("./firewall.json") || File.Exists("./hosts.json"))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("### WARNING ###");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("\nNo restore operation has been performed since the last unblock." +
                            "\nContinuing will likely make no changes to the firewall." +
                            "\nContinuing will result in the loss of un-restored protections to the hosts file if a backup has not been made." +
                            "\n\nAre you sure you want to continue? (Y/N)");
                        retryUserRestoreRisk:
                        userInput = Console.ReadKey().Key;

                        switch (userInput)
                        {
                            case ConsoleKey.Y:
                                Unblock();
                                break;

                            case ConsoleKey.N:
                                Menu();
                                break;

                            default:
                                goto retryUserRestoreRisk;
                        }
                    }
                    else
                    {
                        Unblock();
                    }
                    Menu();
                    break;

                case ConsoleKey.D2:
                    Block();
                    Menu();
                    break;

                case ConsoleKey.D3:
                    HostsBackup();
                    Menu();
                    break;

                case ConsoleKey.D4:
                    if (!File.Exists("./hosts.bak"))
                    {
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("ERROR: No backup hosts file found!");
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        Menu();
                        break;
                    }
                    HostsBackupRestore();
                    Menu();
                    break;

                case ConsoleKey.D5:
                    Process.Start("https://www.gitlab.com/Mace_ya_face/XboxGameServicesUnblocker");
                    Menu();
                    break;

                case ConsoleKey.D6:
                    Environment.Exit(0);
                    break;

                default:
                    goto retryUserMenu;
            } 
        }

        static void Unblock()
        {
            Console.Clear();

            EnableMicrosoftSpyServices();
            FireWallUnblock();
            HostsUnblock();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\nUnblock Successful!");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nPress any key to return to the menu...");
            Console.ReadKey();
        }

        static void Block()
        {
            Console.Clear();

            DisableMicrosoftSpyServices();
            FireWallRestore();
            HostsRestore();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\nProtections restored!");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Press any key to return to the menu...");
            Console.ReadKey();
        }

        static void HostsUnblock()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n### Hosts File ###");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Checking and waiting for hosts file access...");
            while (!IsFileReady("C:/Windows/System32/drivers/etc/hosts")) { }

            Console.SetCursorPosition(45, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Done!");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nSearching for hosts rules...");

            string hostsParsed;
            try
            {
                StreamReader hostsFile = new StreamReader("C:/Windows/System32/drivers/etc/hosts");
                hostsParsed = hostsFile.ReadToEnd();
                hostsFile.Close();
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to read OS hosts file! Attempting roll-back...");
                //Impliment roll-back once code-cleanup complete
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                Environment.Exit(1);
                return;
            }
            var lines = hostsParsed.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            var linesToRestore = new List<string>();
            
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("vortex") || lines[i].Contains("W10Privacy"))
                {
                    string consoleOutput;
                    var commentsStartIndexSpace = lines[i].IndexOf(" ", lines[i].IndexOf(" ") + 1);
                    var commentStartIndexTab = lines[i].IndexOf("\t", lines[i].IndexOf(" ") + 1);
                    var commentStartIndexHash = lines[i].IndexOf("#", lines[i].IndexOf(" ") + 1);
                    var largestIndex = Math.Min(commentsStartIndexSpace, (Math.Min(commentStartIndexTab, commentStartIndexHash)));
            
                    if (largestIndex > 0)
                    {
                        consoleOutput = lines[i].Substring(0, largestIndex);
                    }
                    else
                    {
                        consoleOutput = lines[i];
                    }

                    if(consoleOutput.Length > 95)
                    {
                        consoleOutput = consoleOutput.Substring(0, 93);
                        consoleOutput += "~";
                    }
            
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("\nRemoving hosts rule | " + consoleOutput + "...");
                    linesToRestore.Add(lines[i]);
                    lines[i] = "";
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.SetCursorPosition(25 + consoleOutput.Length, Console.CursorTop - 1);
                    Console.Write("Done!");
                }
            }

            if(linesToRestore.Count < 1)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nNo blocking hosts rules found!");
                return;
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nFlushing JSON to disk...");
            var linesToRestoreJson = JsonConvert.SerializeObject(linesToRestore);
            try
            {
                File.WriteAllText("./hosts.json", linesToRestoreJson);
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to write hosts.json to disk! Attempting roll-back...");
                //Impliment roll-back once code-cleanup complete
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                Environment.Exit(1);
                return;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.SetCursorPosition(24, Console.CursorTop - 1);
            Console.Write("Done!");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nFlushing OS hosts file to disk...");
            while (!IsFileReady("C:/Windows/System32/drivers/etc/hosts")) { }

            try
            {
                StreamWriter unblockedHosts = new StreamWriter("C:/Windows/System32/drivers/etc/hosts");
                foreach (var line in lines)
                {
                    if (line != "") { unblockedHosts.WriteLine(line); }
                }
                unblockedHosts.Close();
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to write OS hosts file changes to disk! Attempting roll-back...");
                //Impliment roll-back once code-cleanup complete
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                Environment.Exit(1);
                return;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.SetCursorPosition(33, Console.CursorTop - 1);
            Console.Write("Done!");
        }

        static void FireWallUnblock()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n### Firewall ###");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Searching for rules...");

            INetFwPolicy2 firewallPolicy;
            try
            {
                firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWPolicy2"));
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nERROR: Failed to connect to Firewall management console!");
                Console.WriteLine("\nPress any key to exit...");
                Console.ReadKey();
                Environment.Exit(1);
                return;
            }
            List<string> firewallRuleList = new List<string>();

            foreach (INetFwRule rule in firewallPolicy.Rules)
            {
                if (rule.Name.Contains("windowsSpyBlocker") || rule.Name.Contains("W10Privacy") || rule.Name.Contains("Blocker MicrosoftTelemetry") || rule.Name.Contains("Blocker MicrosoftExtra") || rule.Name.Contains("Blocker MicrosoftUpdate"))
                {
                    var consoleOutput = rule.Name;
                    if (consoleOutput.Length > 100)
                    {
                        consoleOutput = consoleOutput.Substring(0, 98);
                        consoleOutput += "~";
                    }

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("\nDisabling rule | " + consoleOutput + "...");
                    try
                    {
                        rule.Enabled = false;
                        firewallRuleList.Add(rule.Name);
                    }
                    catch
                    {
                        Console.SetCursorPosition(rule.Name.Length + 20, Console.CursorTop - 1);
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.Write("FAILED!");
                        continue;
                    }
                    
                    Console.SetCursorPosition(rule.Name.Length + 20, Console.CursorTop - 1);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Done!");
                }
            }
                

            if (firewallRuleList.Count < 1)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("No W10Privacy/StevenBlack/WindowsSpyBlocker rules found!");
                return;
            }

            var firewallRulesJson = JsonConvert.SerializeObject(firewallRuleList);
            try
            {
                File.WriteAllText("./firewall.json", firewallRulesJson);
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to write firewall.json to disk! Attempting roll-back...");
                //Impliment roll-back once code clean-up has been done
                Console.WriteLine("\nPress any key to exit...");
                Console.ReadKey();
                Environment.Exit(1);
                return;
            }
        }

        static void EnableMicrosoftSpyServices()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("### Microsoft Spy Services ###");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Starting CUET service...");

            ServiceController service = new ServiceController("DiagTrack");
            try
            {
                using (var mo = new ManagementObject(string.Format("Win32_Service.Name=\"{0}\"", "DiagTrack")))
                {
                    mo.InvokeMethod("ChangeStartMode", new object[] { "Automatic" });
                }

                if ((service.Status.Equals(ServiceControllerStatus.Stopped)) || (service.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    service.Start();
                }
            }
            catch { }

            Console.SetCursorPosition(24, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Done!");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nStarting Gaming Services...");

            try
            {
                service = new ServiceController("GamingServicesNet");

                Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\GamingServicesNet", "Start", 2, RegistryValueKind.DWord);

                if ((service.Status.Equals(ServiceControllerStatus.Stopped)) || (service.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    service.Start();
                }

                service = new ServiceController("GamingServices");

                Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\GamingServices", "Start", 2, RegistryValueKind.DWord);

                if ((service.Status.Equals(ServiceControllerStatus.Stopped)) || (service.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    service.Start();
                }
            }
            catch { }
            

            Console.SetCursorPosition(27, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Done!");
        }

        static void DisableMicrosoftSpyServices()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("### Microsoft Spy Services ###");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Stopping CUET service...");

            ServiceController service = new ServiceController("DiagTrack");

            try
            {
                using (var mo = new ManagementObject(string.Format("Win32_Service.Name=\"{0}\"", "DiagTrack")))
                {
                    mo.InvokeMethod("ChangeStartMode", new object[] { "Disabled" });
                }

                if (!service.Status.Equals(ServiceControllerStatus.Stopped) && !service.Status.Equals(ServiceControllerStatus.StopPending))
                {
                    service.Stop();
                }
            }
            catch { }
            

            Console.SetCursorPosition(24, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Done!");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nStopping Gaming Services...");

            try
            {
                service = new ServiceController("GamingServicesNet");

                Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\GamingServicesNet", "Start", 4, RegistryValueKind.DWord);

                if (!service.Status.Equals(ServiceControllerStatus.Stopped) && !service.Status.Equals(ServiceControllerStatus.StopPending))
                {
                    service.Stop();
                }

                service = new ServiceController("GamingServices");

                Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\GamingServices", "Start", 4, RegistryValueKind.DWord);

                if (!service.Status.Equals(ServiceControllerStatus.Stopped) && !service.Status.Equals(ServiceControllerStatus.StopPending))
                {
                    service.Stop();
                }
            }
            catch { }
            

            Console.SetCursorPosition(27, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Done!");
        }

        static void FireWallRestore()
        {
            if (!File.Exists("./firewall.json")) { return; }

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n### Firewall ###");

            INetFwPolicy2 firewallPolicy;
            try
            {
                firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWPolicy2"));
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to connect to the Windows FireWall! Please restore FireWall rules manually and then delete firewall.json");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nPress any key to continue...");
                Console.ReadKey();
                return;
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Parsing Firewall JSON...");
            string firewallRulesJson;
            try
            {
                firewallRulesJson = File.ReadAllText("./firewall.json");
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to read firewall.json! Please restore FireWall rules manually and then delete firewall.json");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nPress any key to continue...");
                Console.ReadKey();
                return;
            }
            var firewallRules = JsonConvert.DeserializeObject<List<string>>(firewallRulesJson);

            Console.SetCursorPosition(24, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Done!");

            foreach (string rule in firewallRules)
            {
                var consoleOutput = rule;
                if (consoleOutput.Length > 101)
                {
                    consoleOutput = consoleOutput.Substring(0, 99);
                    consoleOutput += "~";
                }

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nEnabling rule | " + consoleOutput + "...");
                try
                {
                    var r = firewallPolicy.Rules.Item(rule);
                    r.Enabled = true;
                }
                catch
                {
                    Console.SetCursorPosition(rule.Length + 19, Console.CursorTop - 1);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("FAILED!");
                }
                Console.SetCursorPosition(rule.Length + 19, Console.CursorTop - 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Done!");
            }

            try
            {
                File.Delete("./firewall.json");
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to delete firewall.json! Please delete firewall.json manually");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nPress any key to continue...");
                Console.ReadKey();
                return;
            }
        }

        static void HostsRestore()
        {
            if (!File.Exists("./hosts.json")) { return; }

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n### Hosts File ###");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Parsing Hosts JSON...");
            string hostsFileLinesJson;
            try
            {
                hostsFileLinesJson = File.ReadAllText("./hosts.json");
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to read hosts.json! Please restore hosts file manually, and delete hosts.json after manual restoration");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nPress any key to continue...");
                Console.ReadKey();
                return;
            }
            var hostsFileLines = JsonConvert.DeserializeObject<List<string>>(hostsFileLinesJson);

            Console.SetCursorPosition(21, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Done!");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nChecking and waiting for hosts file access...");
            while (!IsFileReady("C:/Windows/System32/drivers/etc/hosts")) { }

            Console.SetCursorPosition(45, Console.CursorTop - 1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Done!");

            using(StreamWriter sw = File.AppendText("C:/Windows/System32/drivers/etc/hosts"))
            {
                foreach(var line in hostsFileLines)
                {
                    string consoleOutput;
                    var commentsStartIndexSpace = line.IndexOf(" ", line.IndexOf(" ") + 1);
                    var commentStartIndexTab = line.IndexOf("\t", line.IndexOf(" ") + 1);
                    var commentStartIndexHash = line.IndexOf("#", line.IndexOf(" ") + 1);
                    var largestIndex = Math.Min(commentsStartIndexSpace, (Math.Min(commentStartIndexTab, commentStartIndexHash)));

                    if(largestIndex > 0)
                    {
                        consoleOutput = line.Substring(0, largestIndex);
                    }
                    else
                    {
                        consoleOutput = line;
                    }

                    if (consoleOutput.Length > 94)
                    {
                        consoleOutput = consoleOutput.Substring(0, 92);
                        consoleOutput += "~";
                    }

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("\nRestoring hosts rule | " + consoleOutput + "...");
                    try
                    {
                        sw.WriteLine(line);
                    }
                    catch
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.SetCursorPosition(26 + consoleOutput.Length, Console.CursorTop - 1);
                        Console.Write("FAILED!");
                    }
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.SetCursorPosition(26 + consoleOutput.Length, Console.CursorTop - 1);
                    Console.Write("Done!");
                }
                sw.Close();
            }
            try
            {
                File.Delete("./hosts.json");
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to delete hosts.json! Please delete hosts.json manually");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nPress any key to continue...");
                Console.ReadKey();
                return;
            }
        }

        static void HostsBackup()
        {
            Console.Clear();

            if (File.Exists("./hosts.bak"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("### WARNING ###");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Continuing will overwrite your existing backup.\nAre you sure you want to continue? (Y/N)");
                retryUserBackupRisk:
                var userInput = Console.ReadKey().Key;

                switch (userInput)
                {
                    case ConsoleKey.Y:
                        Console.Clear();
                        break;

                    case ConsoleKey.N:
                        Menu();
                        break;

                    default:
                        goto retryUserBackupRisk;
                }
            }

            while (!IsFileReady("C:/Windows/System32/drivers/etc/hosts")) { }

            try
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Creating hosts file backup...");
                if (File.Exists("./hosts.bak")){ File.Delete("./hosts.bak"); }
                File.Copy("C:/Windows/System32/drivers/etc/hosts", Path.Combine("./hosts.bak"));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.SetCursorPosition(29, Console.CursorTop - 1);
                Console.Write("Done!");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\n\n Press any key to continue...");
                Console.ReadKey();
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to copy hosts file for backup!");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nPress any key to continue...");
                Console.ReadKey();
                return;
            }
        }

        static void HostsBackupRestore()
        {
            Console.Clear();
            while (!IsFileReady("C:/Windows/System32/drivers/etc/hosts")) { }

            try
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Restoring hosts file backup...");
                File.Delete("C:/Windows/System32/drivers/etc/hosts");
                File.Copy("./hosts.bak", "C:/Windows/System32/drivers/etc/hosts");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.SetCursorPosition(30, Console.CursorTop - 1);
                Console.Write("Done!");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\n\n Press any key to continue...");
                Console.ReadKey();
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\nERROR: Failed to restore hosts file from backup!");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("\nPress any key to continue...");
                Console.ReadKey();
                return;
            }
        }

        public static bool IsFileReady(string filename)
        {
            try
            {
                using (FileStream inputStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                    return inputStream.Length > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
