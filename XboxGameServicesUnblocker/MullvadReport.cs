﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XboxGameServicesUnblocker
{
    public class MullvadReport
    {
        public string Ip { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        [JsonProperty(PropertyName = "mullvad_exit_ip")]
        public bool MullvadExitIp { get; set; }
        [JsonProperty(PropertyName = "mullvad_exit_ip_hostname")]
        public string MullvadExitIpHostName { get; set; }
        [JsonProperty(PropertyName = "mullvad_server_type")]
        public string MullvadServerType { get; set; }
        public IsBlacklisted Blacklisted { get; set; }
    }

    public class IsBlacklisted
    {
        public bool Blacklisted { get; set; }
        public IList<BlacklistInfo> Results { get; set; }
    }

    public class BlacklistInfo
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public bool BlackListed { get; set; }
    }
}
